FROM node:alpine as builder

ENV PORT 3000

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN apk add jq

# Installing dependencies
COPY package*.json /usr/src/app/

# Update version
COPY update-version.sh .
RUN ./update-version.sh

RUN yarn install

# Copying source files
COPY . /usr/src/app

# Building app
RUN npm run build
EXPOSE 3000

# Running the app
CMD "npm" "run" "dev"